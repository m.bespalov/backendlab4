<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    print("hi");
    if (!empty($_GET['save'])) {
        print('Спасибо, результаты сохранены.');
    }
    include('form.php');
    exit();
}

$errors = FALSE;
if (empty($_POST['fio'])) {
    print('Заполните имя.<br/>');
    $errors = TRUE;
}

if (empty($_POST['email'])) {
    print('Заполните email.<br/>');
    $errors = TRUE;
} else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    print('Email введен некорректно.<br/>');
    $errors = TRUE;
}

if ($errors) {
    exit();
}

$user = 'u40075';
$pass = '8514870';
$abil = implode(",", $_POST['abilities']);


try {
    $db = new PDO('mysql:host=localhost;dbname=u40075', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $stmt = $db->prepare("INSERT INTO form SET fio = ?, email = ?, year_ = ?, sex = ?, limbs = ?, bio = ?, accept = ?");
    $stmt->execute([$_POST['fio'], $_POST['email'], $_POST['year_'], $_POST['sex'], $_POST['limbs'], $_POST['bio'], $_POST['accept']]);
    $id_user = $db->lastInsertId();

    $stmt1 = $db->prepare("INSERT INTO abilities SET id = ?, abil = ?");
    $stmt1->execute([$id_user, $abil]);
} catch (PDOException $e) {
    print('Error : ' . $e->getMessage());
    exit();
}
