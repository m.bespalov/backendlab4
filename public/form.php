<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>TASK 4</title>
    <style>
        .error {
            border: 1px red solid !important;
        }
    </style>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="js/count.js" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<?php
if (!empty($messages)) {
    print('<div id="messages">');
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}
?>
<div class="contentblocks">
    <div class="container-sm theme-list py-3 pl-0 mb-3">
        <div class="d-flex flex-column align-items-center flex-wrap">
            <h2 class="text-center" id="form">Форма</h2>
            <form class="d-block p-2" action="index.php" method="POST">
                <label>
                    <label>Your name</label>
                    <input name="fio" <?php if ($errors['fio']) print('class="error"'); ?>
                           value="<?php print($NAME); ?>"/>
                </label><br/>
                <label>
                    Email:
                    <input name="email" <?php if ($errors['email']) print('class="error"'); ?>
                           value="<?php print($EMAIL); ?>"
                           type="email"/>
                </label><br/>
                <label>
                    Год рождения:<br/>
                    <select name="year_">
                        <?php for ($i = 1900; $i < 2021; $i++) {
                            print('<option value="');
                            print($i);
                            if ($YEAR == $i) print('" selected="selected">');
                            else print('">');
                            print($i);
                            print('</option>');
                        } ?>
                    </select>
                </label><br/>
                Пол:<br/>
                <label><input type="radio" <?php if ($SEX == "М") print("checked"); ?>
                              name="sex" value="М"/>
                    М</label>
                <label><input type="radio" <?php if ($SEX == "Ж") print("checked"); ?>
                              name="sex" value="Ж"/>
                    Ж</label><br/>
                Количество конечностей:<br/>
                <?php for ($i = 1; $i <= 4; $i++) {
                    print('<label><input type="radio" name="limb"');
                    if ($LIMB == $i) print('checked="checked" value="');
                    else print('value="');
                    print($i);
                    print('"/>');
                    print($i);
                    print('</label>');
                }
                ?>
                <br/>
                <label>
                    Способности:
                    <br/>
                    <select name="power[]"
                            multiple="multiple">
                        <option value="ab_fly" <?php if ($FLY) print('selected'); ?>>Левитация</option>
                        <option value="ab_god" <?php if ($GOD) print('selected'); ?> >Бессмертие</option>
                        <option value="ab_clip" <?php if ($CLIP) print('selected'); ?> >Прохождение сквозь стены</option>
                    </select>
                </label><br/>
                <label>
                    Биография:<br/>
                    <textarea name="bio"><?php print($BIO); ?> </textarea>
                </label><br/>
                <label><input type="checkbox"
                              name="check" required/>
                    С контрактом ознакомлен</label><br/>
                <input type="submit" value="Отправить"/>
            </form>
        </div>
    </div>
</div>
</body>
</html>
